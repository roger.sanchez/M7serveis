---
Title: Consideracions de seguretat
weight: 6
Description: "Servidors d'aplicacions web. Servei web. Apache"
---


## Altres directives Apache

-   Per indicar els privilegis que es pot tenir dintre un directori, es fa servir les directives següents (que poden anar dintre un directiva <Directory> o bé trobar-se a un fitxer de text ".htaccess" que estigui gravat al directori on volem accedir):

- Options: Indica accions que es poden realitzar al directori. Com ara:
- Indexes: Permet veure el llistat del directori si no hi ha fitxer index.html
- MultiViews: Per mostrar contingut negociat entre servidor i client (complex d'explicar, poseu-ho per defecte).
- FollowSymLinks: Permet seguir un hiperenllaç quan es pica a sobre.       
- AllowOverride: Indica quines directives es poden sobreescriure si al directori es troba un fitxer anomenat .htaccess que contingui text amb definició de directives a dintre.
- None: Indicaria que encara que el fitxer .htaccess existeix, no es tindrà en compte.
- Options=Indexes,Multiviews   (indicaria que es permet sobreescriure les opcions Indexes i Multiviews).
Normalment posarem "None" i no deixarem el control del lloc virtual al  propietari del directori (que és el que passa si posem "AllowOverride all" o deixem alguna directiva que es pugui sobreescriure).
- Order: Indica l'ordre en que s'avaluen les línies que comencen per Deny o Allow. Ex: deny,allow  (primer avalua les deny i després les allow).

	-  Allow,Deny Primer, totes les directives Allow són evaluades; al menys una ha de coincidir, o la petició serà rebutjada. Després, totes les directives Deny són avaluades. Si alguna coincideix, la petició serà rebutjada. Al final, qualsevol pertició que no coincideixi amb una directiva Allow o Deny serà denegada per defecte.
	- Deny,Allow    Primer, totes les directives Deny són evaluades; si alguna coincideix, la petició serà denegada a no ser que també coincideixi amb una directiva Allow. Qualsevol petició que no coincideixi amb alguna directiva Allow or Deny serà permesa.

-   Deny: Denegar accés des de les adreces que es posin al "from". Ex: Deny from all (no permet accés des de cap adreça).
-  Allow: Permetre accés de de les adreces que es posin al "from". Ex: Allow from 127.0.0.0/255.0.0.0 192.168.0.1/255.255.255.0

## Autoritzar un directori

- Per tenir un directori al que només hi pot accedir els usuaris autoritzats, farem el següent:

- Seguiu les instruccions que es troben a: http://httpd.apache.org/docs/2.4/howto/auth.html

### Creació dels fitxers

-  Bàsicament, es tracta de crear un fitxer d'usuaris amb passwords. al directori on volem crear el fitxer, per exemple a /usr/local/apache/passwd

~~~
$roger@roger-desktop:~$ sudo htpasswd -c secret usuari

[sudo] password for roger:
New password:
Re-type new password:
Adding password for user usuari

roger@roger-desktop:~$ cat secret
usuari:$apr1$Ovsvd5mg$3A7xY44qPFvINpjqFjyHN1
~~~

- Si volem crear més usuaris, pels següents usuaris NO posem el paràmetre -c a htpasswd.
- I de posar dintre el directori que volem protegir:

### Configuració

- Tota la configuració ha d'estar entre les etiquetes directory
- ex:


~~~
<Directory "/var/www/html/dir">

...

</Directory>
~~~

- Primer especifiquems que utilitza autenticació i la frase que ens sortirà a l'alerta


~~~
AuthType Basic
AuthName "Aquesta zona està restringida, es requereix usuari i password"
~~~

- Especifiquem que utilitzarem autenticació per fitxer i on es troba

~~~
AuthBasicProvider file
AuthUserFile /usr/local/apache/passwd/secret
~~~


- [OPCIONAL]  si volem utilitzar grups haurim de tenir un fitxer amb una linea per grup amb el format nomgrup1: usuari1 usuari2 ... i especificar el path del fitxer al directory amb el paràmetre

~~~
AuthGroupFile /usr/local/apache/passwd/group
~~~

- [OPCIONAL] al utilitzar grups podria ser que necessitem activar el modul authz_groupfile

~~~
a2enmod authz_groupfile
~~~


- Finalment hauriem d'escollir si volem autenticar un usuari concret, tots els usuaris del fitxers de contrasenyes o tots els que pertanyin a un grup concret amb el paràmetre respectiu:

~~~
Require user roger
Require valid-user
Require group nomgrup1

~~~

## SSL

- Seguiu les indicacions del següent tutorial (explica com configurar Apache per tenir el Moodle en web segura)

- http://docs.moodle.org/24/en/Apache

- https://www.digicert.com/ssl-support/apache-multiple-ssl-certificates-using-sni.htm


## Pàgines error

- Podeu veure el manual oficial https://httpd.apache.org/docs/2.4/custom-error.html
- Format
	- Si poses url es redirigeix allà
	- Si poses frase surt aquella

~~~
ErrorDocument <3-digit-code> <action>
~~~

- exemple:

~~~
ErrorDocument 500 "Sorry, our script crashed. Oh dear"
ErrorDocument 500 /cgi-bin/crash-recover
ErrorDocument 500 http://error.example.com/server_error.html
ErrorDocument 404 /errors/not_found.html
ErrorDocument 401 /subscription/how_to_subscribe.html
~~~

## Altres consideracions de seguretat

- https://httpd.apache.org/docs/2.4/misc/security_tips.html
