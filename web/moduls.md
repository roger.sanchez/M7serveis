---
Title: Moduls Apache
weight: 5
Description: "Servidors d'aplicacions web. Servei web. Apache"
---


## Tenir espais web per a cada usuari de manera automàtica

- Activarem el mòdul "userdir", escrivint a entorn de comandes:

~~~
a2enmod userdir
~~~

- Cada usuari, al seu directori personal ha de tenir un directori anomenat "public_html". Tot el que posi allà es veurà com un lloc web (haurem de posar-hi algun fitxer index.html) des del client web:

~~~
http://<ip-o-nom-de-servidor>/~<nom-usuari>
~~~

- exemple:

~~~
http://172.16.0.2/~jperez
http://jazztel.com/~jperez
~~~

>NOTA: Si volem que tots els usuaris que creem tinguin en el moment de crear-los el directori "public_html" i un fitxer index.html a dintre per defecte, haurem d'anar a /etc/skel i crear allà el directori i el fitxer, abans de crear els usuaris.

- Aquest directori /etc/skel s'utilitza de plantilla quan es crea el directori personal d'un usuari.
>ATENCIÓ: Per defecte Apache no permet l'execució de php5 pels directoris dels usuaris (en el cas que ja funcioni, no feu res). Si volem fer-ho, cal editar el fitxer /etc/apache2/mods-available/php5.conf i comentar les línies a partir de <IfModule mod_userdir.c> (tal com indica el comentari en anglès :) )

~~~
<IfModule mod_php5.c>

   <FilesMatch "\.ph(p3?|tml)$">

       SetHandler application/x-httpd-php

   </FilesMatch>

   <FilesMatch "\.phps$">

       SetHandler application/x-httpd-php-source

   </FilesMatch>

# To re-enable php in user directories comment the following lines

# (from <IfModule ...> to </IfModule>.) Do NOT set it to On as it

# prevents .htaccess files from disabling it.

   <IfModule mod_userdir.c>

       <Directory /home/*/public_html>

              php_admin_value engine Off

       </Directory>

   </IfModule>

</IfModule>
~~~
