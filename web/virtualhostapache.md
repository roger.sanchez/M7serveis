---
Title: Virtual Hosts Apache2
weight: 4
Description: "Servidors d'aplicacions web. Servei web. Apache"
---


## Passes per a crear un virtual host:

1. Copiar el fitxer default del directori /etc/apache2/sites-available amb un altre nom (el nom és el que vulguem, a l'exemple posem el de la web): cp /etc/apache2/sites-available/default /etc/apache2/sites-available/acme.conf
2. Editar el nou fitxer amb els paràmetres que necessitem. Esborrem les directives que no necessitem. Normlament, tocarem la directiva <VirtualHost> per posar una adreça IP i /o un port diferents.
3. A més, canviarem la ubicació del directori arrel del lloc virtual (DocumentRoot) i la directiva Directory associada per indicar els privilegis d'accés.
4. Si la carpeta que conté els fitxers del lloc web no existeix, l'haurem de crear i ficar a dintre el contingut del lloc web (per exemple els fitxers de Joomla, encara que per provar, posarem un fitxer index.html amb un missatge que indiqui on som).
    MAI no posarem la carpeta del nou lloc web dintre de /var/www/html ja que és l'arrel del lloc web per defecte. Crearem una carpeta "germana" (per comoditat posarem tots els llocs web a la carpeta /var/www, però podem posar-los on vulguem), per exemple: mkdir /var/www/acme".
5. Comprovem sintaxi dels fitxers de configuració de l'Apache (inclòs el que hem creat) amb: "apache2ctl configtest" (mireu també altres opcions de la comanda, per exemple "fullstatus").
6. Si s'ha canviat el numero de port del lloc virtual, i Apache no escolta en aquest port, cal anar al fitxer /etc/apache2/ports.conf i afegir una directiva "Listen" amb el nou port. Per exemple "Listen 8008".
7. Activar el nou lloc virtual: Això crea un enllaç tou (soft-link) des del directori sites-enabled cap al fitxer que acabem de crear a sites-available. En aquest directori es troben tots els llocs virtuals activats. Executem "a2ensite acme.conf"
8. Activar els móduls que necessitem si cal. Per exemple, per suport de php5 cal fer: "a2enmod php5". Si posem a2enmod sense paràmetre, ens diu les opcions de que disposem.
9. Reiniciar el servidor Apache: sudo service apache2 restart (o reload).
10. Només si és necessari, podem desactivar un lloc virtual amb "a2dissite acme.conf" i desactivar un mòdul amb "a2dismod php5"

## Default Virtual Host

- El contingut del fitxer /etc/apache2/sites-available/default:
- Capçalera de virtual host, ha d'existir el tancament al final del fitxer

~~~
 </VirtualHost>

<VirtualHost *:80>

            # Cal posar * (qualsevol IP i port 80 per defecte) o bé

            # *:port (qualsevol IP, amb port determinat) o bé

            # IP (adreça específica, port per defecte, serà 80) o bé

            # IP:port (IP i port específic)


    ServerAdmin webmaster@localhost   # Adreça de correu de l'admin

    DocumentRoot /var/www             # Directori arrel del lloc virtual, on es  trobarà el fitxer index.html

    <Directory />                     # Definició de privilegis sobre el directori
        Options FollowSymLinks        # Opcions que es poden fer al directori

                                      # http://httpd.apache.org/docs/2.4/en/mod/core.html#options
        AllowOverride None            # Permetre sobreescriure directives del directori amb

                                      # les que apareguin al fitxer .htacces (si existeix)

                                      # "None" indica que no permet sobreescriure cap d'elles

    </Directory>

    <Directory /var/www/>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride None
        Order allow,deny              # Ordre en que es processen les directives allow i deny
        allow from all                # Adreces IP des de les que es permet l'accés al directori
    </Directory>

    ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
    <Directory "/usr/lib/cgi-bin">
        AllowOverride None
        Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
        Order allow,deny
        Allow from all
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.log  # Directori on es troba el fitxer de logs

    # Nivell de logs: Possible values include: debug, info, notice, warn, error, crit,
    # alert, emerg.
    LogLevel warn

    # Logs del lloc virtual, si en volem un d'especial només per al lloc
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    # Defineix un àlias del directori que hi ha entre cometes, de manera que es veurà penjant

    # del directori definit a DocumentRoot, en aquest cas, es veuria com si estigués a:

    # http://ip/doc (apareixerà el contingut que hi hagi a /usr/share/doc
    Alias /doc/ "/usr/share/doc/"
    <Directory "/usr/share/doc/">
        Options Indexes MultiViews FollowSymLinks
        AllowOverride None
        Order deny,allow
        Deny from all
        Allow from 127.0.0.0/255.0.0.0 ::1/128
    </Directory>

</VirtualHost>
~~~


## Tenir més d'un lloc web al mateix servidor Apache

- Si volem tenir llocs web que semblin diferents (en realitat només hi hauria un) podem posar cada lloc web a un directori virtual. Això es pot aconseguir:
- Creant un subdirectori que pengi del directori de DocumentRoot i posant a dintre els fitxers de lloc que volem veure.
- Per exemple, si el directori de DocumentRoot és /var/www/html i volem tenir un Joomla i un Moodle (CMS d'exemple), crearem a dintre /var/www/html/moodle i /var/www/html/joomla i hi posarem els fitxers dels CMS esmentats.

- Per accedir a cada un d'aquests llocs, al navegador client posarem:

~~~
http://<ip-servidor>/joomla

http://<ip-servidor>/moodle
~~~

- Amb la directiva Alias, que fa que dintre de DocumentRoot sembli que hi ha un directori nou que en realitat no es troba dintre del directori de DocumentRoot.
-  Per exemple, si el directori de DocumentRoot és /var/www/html i volem tenir un Joomla i un Moodle (CMS d'exemple), si posem els fitxers a /var/joomla i a /var/moodle, posarem dues directives Alias amb la Directory associada:

~~~
Alias /joo "/var/joomla/" (joo serà el nom del directori que sembla penjar del DocumentRoot)

<Directory "/var/joomla/">

...

</Directory>

Alias /mdl "/var/moodle/"

<Directory "/var/moodle/">

...

</Directory>

Per accedir a cada un d'aquests llocs, al navegador client posarem:

http://<ip-servidor>/joo

http://<ip-servidor>/mdl
~~~

## Varis Virtual Host al mateix servidor

- Podem tenir més d'un virtual host al mateix servidor Apache. Evidentment han de diferenciar-se en alguna cosa uns dels altres. Com a mínim tindrem el virtualhost per defecte quan instal·lem Apache.


### Mètode 1: Adreça IP

- Posem una adreça IP diferent per a cada lloc virtual.
- Requereix que el Servidor tingui més d'una adreça IP.
- Només hem de canviar a la directiva <VirtualHost IP> l'adreça IP per a que sigui diferent de la dels altres VirtualHost.
- Al navegador posarem, per accedir a un lloc o l'altre:

~~~
http://<IP1-server> (pel primer virtualhost)

http://<IP2-server> (pel segon virtualhost).
~~~

### Mètode 2:  port

- Port: Posem un número de port diferent per a cada lloc virtual.
- Només hem de canviar a la directiva <VirtualHost IP> i afegir

~~~
 <VirtualHost IP:port> amb el número de port que correspongui.
~~~

- Requereix que Apache escolti en tots els ports que indiquem als virtual hosts. Cal al fitxer /etc/apache2/ports.conf una directiva Listen per a cada port.

- Al navegador posarem, per accedir a un lloc o l'altre:

~~~
http://<IP-server>:port1 (pel primer virtualhost)

http://<IP-server>:port2 (pel segon virtualhost).

~~~

### Mètode 3: Nom

- Posem un nom de host DNS diferent per cada lloc virtual.
- Es fan grups de VirtualHost amb la mateixa capçalera <VirtualHost>. Per exemple, tots els VirtualHost tindran la capçalera:

~~~
<VirtualHost 172.16.0.1>
~~~

- (es pot fer amb la capçalera per defecte <VirtualHost *:80>, si volem)

- Al fitxer ports.conf, posarem una línia "NameVirtualHost 172.16.0.1" (o sigui, el que vé darrera de VirtualHost a totes les capçaleres).

- ATENCIÓ: Requereix que els noms de host que fem servir es puguin resoldre (és a dir, obtenir l'adreça IP a partir del nom de host).
- Com que tots els VirtualHost tenen la mateixa capçalera, per diferenciar-los posem a dintre de cada VirtualHost una directiva ServerName, que indica el nom DNS del virtualhost:

~~~
<VirtualHost 172.16.0.1>

....

ServerName www.acme.com

ServerAlias web.acme.com # si volem tenir més d'un nom pel aquest lloc virtual, si no, no cal posar-la

....

</VirtualHost>

<VirtualHost 172.16.0.1>

....

ServerName altrelloc.acme.com

...

</VirtualHost>
~~~

- Al navegador posarem, per accedir a un lloc o l'altre:

~~~
http://www.acme.com (pel primer virtualhost, o bé, pel mateix lloc, hhtp://web.acme.com)

http://altrelloc.acme.com (pel segon virtualhost).
~~~
