---
Title: Apache2
weight: 2
Description: "Servidors d'aplicacions web. Servei web. Apache"
---



## Característiques

- Basat en NCSA HTTP
- Nom
	- A pached server
	- Apaches (últims en rendir-se)


- Permet
	- Instal·lació de mòduls
	- Virtual Hosts
		- +>1 web site en una sola màquina
		- Pot estar basant en IP o en nom de màquina
		- L'usuari final desconeix aquest fet


## Instal·lació

- El servidor web hauria de tenir una adreça de la xarxa amb ip fixa.

- Comproveu que al port 80 està escoltant el servidor Apache, obriu un altre terminal i feu:

~~~
sudo watch "netstat -atunp"
~~~


- Els paquets a instal·lar són:

~~~
apache2 apache2-docs
~~~




## Logs i tests

- Per mirar els logs, en una altra terminal (finestra o sessió nova), obrim el fitxer de logs del sistema:

~~~
    sudo tail -f /var/log/apache2/access.log
~~~

- Veurem una sortida de l'estil següent. Cada línia és una petició web (GET en aquest cas) i conté informació com la data, el nom de recurs i navegador client.

~~~
127.0.0.1 - - [07/Jan/2015:10:12:12 +0100] "GET /secret/ HTTP/1.1" 401 712 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1"

127.0.0.1 - joan [07/Jan/2015:10:12:16 +0100] "GET /secret/ HTTP/1.1" 200 408 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1"
~~~

- Veurem els missatges d'error o avisos del servidor web:

~~~
sudo tail -f /var/log/apache2/error.log

[Mon Jan 07 11:58:43 2015] [notice] Apache/2.2.22 (Ubuntu) PHP/5.3.10-1ubuntu3 with Suhosin-Patch configured -- resuming normal operations
[Mon Jan 07 12:50:52 2015] [error] [client 127.0.0.1] File does not exist: /var/www/favicon.ico
[Mon Jan 07 13:39:07 2015] [notice] caught SIGTERM, shutting down
[Mon Jan 07 13:39:08 2015] [notice] Apache/2.2.22 (Ubuntu) PHP/5.3.10-1ubuntu3 with Suhosin-Patch configured -- resuming normal operations
[Mon Jan 07 13:41:54 2015] [error] [client 172.16.12.50] File does not exist: /var/www/favicon.ico
[Mon Jan 07 13:42:33 2015] [error] [client 172.16.12.50] File does not exist: /var/www.moodle.com/favicon.ico
[Mon Jan 07 13:43:29 2015] [error] [client 172.16.12.50] File does not exist: /var/www.joomla.com/favicon.ico
[Mon Jan 07 14:41:15 2015] [notice] caught SIGTERM, shutting down
~~~

- Per provar que el servei funciona, connectarem des del propi servidor a "localhost" mitjançant un client web (http://localhost). Si tot va bé, ens apareixerà una pàgina de benvinguda amb la llegenda "It works".
- Per comprovar que funciona phpmyadmin, escriurem: http://localhost/phpmyadmin
- Per comprovar que funciona el servei a través de la xarxa, mirarem d'obtenir les pàgines web anteriors des d'un equip client, posant l'adreça IP del servidor.
