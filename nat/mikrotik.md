---
Title: Configuració de NAT a Mikrotik / RouterOS
weight: 10
Description: "Configuració de NAT a Mikrotik / RouterOS"
---

## Prèvia

Les credencials són:

~~~
user: admin
password: sense password
~~~

- En un entorn mikrotik físic per la IP del router a la LAN és **192.168.88.1** i ofereix la xarxa **192.168.88.0/24** via `DHCP`

- En la OVA de  RouterOS pots entrar per el terminal  cercar la IP amb la següent comanda i després accedir via `SSH`

```
ip address print
```

## Services

- Pots tenir la necessitat de modificar, desactivar o activar un servei del router. Per exemple, quan vols que desde la xarxa pública es pugui accedir a una web interna enlloc de mostrar la configuració web del router.

- Per modificar / habilitar / deshabilitar un servei del router

![Serveis](../imgs/serveis.png)

## Reenviament de ports


- Per fer el reenviament de ports  desde la `cli` :

```
/ip firewall nat add chain=dstnat action=dst-nat in-interface=[INTERFICIE]  dst-port=[PORT_ROUTER]
protocol=tcp to-address=[IP_SERVER] to-ports=[PORT_SERVER]
```

- Si volguessis fer l'anterior però filtrant que només s'hi pugui accedir desde una única IP pots utilitzar el paràmetre **src-address**

```
/ip firewall nat add chain=dstnat action=dst-nat in-interface=[INTERFICIE] dst-port=[PORT_ROUTER]
protocol=tcp to-address=[IP_SERVER] to-ports=[PORT_SERVER] src-address=![IPPERMESES]
```


- via entorn gràfic

![](../imgs/nat1.png)

---

![](../imgs/nat2.png)

---

![](../imgs/nat3.png)



## Firewall: bloquejar protocols

- Per fer un bloqueig amb la `cli`

```
[admin@RouterOS] > /ip firewall filter add chain=input protocol=[PROTOCOL] action=drop
```
- Per fer un bloqueig amb la `gui`

![](../imgs/firewall.png)



![](../imgs/firewall2.png)

---

![](../imgs/firewall3.png)
