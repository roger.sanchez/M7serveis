---
Title: Interconnexió xarxes públiques i privades
weight: 4
Description: "INterconnexió entre xarxes públiques i privades. ST"
---

## Network Address Translation

- NAT és un procés de traducció d'adreçes entre dues xarxes.
- En aquest procés es modifiquen les adreçes Ipv4 en el `router`
- Hi ha varis tipus de `NAT`

## DNAT i Port Forwarding

- `DNAT` és un tipus de NAT on el que es modifica de forma automàtica i transparent l'adreça destí d'un paquet.
- Normalment s'utilitza per a publicar serveis presents en una xarxa privada a una xarxa pública. Encara que també es podria oferir entre xarxes privades.
- En aquest cas l'adreça pública del servei serà una adreça pública configurada al router.
- El router fa una redirecció  `socket` a `socket` de  **IP pública:port**  cap a **ip privada:port** sent la **IP pública** una IP del router i la **IP privada** on estan configurats els serveis.
- Els clients faran les peticions a la  **IP pública:port** però la rebrà **ip privada:port** sent el router un transmisor.
- D'aquesta manera la xarxa privada queda oculta a la xarxa públic només sent accessible un port concret d'una màquina concreta

### Exemple

- Un router connectat a Internet i a una xarxa Local 192.168.1.0/24 on ell té configurades les IP 6.6.6.6 i 192.168.1.1 i el server la 192.168.1.8
- Es pública a Internet la resolució de noms `myweb.com IN A 6.6.6.6`
- Es redirecciona el tràfic del port 80 (HTTP) del router cap el port 80 del servidor

{{< mermaid >}}
graph LR
  A[Client] -->|Desti: myweb.com 6.6.6.6:80| B(Router)
  B -->|Desti: myweb.com 192.168.1.8:80| C[Servidor]  
{{< /mermaid >}}


{{< tip  >}}
De cara al client Internet el servidor està situat a 6.6.6.6:80 i desconeix que hi ha xarxa entre 6.6.6.6 i el veritable servidor
{{< /tip >}}
