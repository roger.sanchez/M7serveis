---
Title: Protocols Web
weight: 1
Description: "Servidors d'aplicacions web. Servei web. Apache"
---





## WWW

- World Wide Web , 1989, CERN

## HTTP

- hyperText transfer protocol
- TCP/IP -> Unix based
- Transfereix
	- Documents HTML
	- Documents multimedia
	- scripts cgi

### URL

- Uniform Resource Locator

![](../imgs/20181025-091937.png)



### Ports

- HTTP: 80
- HTTPS: 443

### Tipus de peticions

- GET: Demana recursos a un servidor
- POST: Envia dades al servidor
- HEAD: Sol·licita informació d'un objecte

### Headers HTTP

- [Llista de Headers][2c9b489f]
- ![](../imgs/20190809-102253.png)
- HTTP protocol sense estat->Cookies
   - Segons llei és obligatori pedir permís
   - Podem configurar el navegador per a configurar la política de cookies
- HTTPS = HTTP +SSL

  [2c9b489f]: http://en.wikipedia.org/wiki/List_of_HTTP_header_fields "Llista de Headers"


### Procés


{{< mermaid >}}
sequenceDiagram
    participant Client
    participant Servidor
    Note right of Servidor: El servidor obre els ports de TCP 80(HTTP) i 443(HTTPS) i espera peticions
    Client-->>Servidor: Envia petició GET al servidor amb les capçalers HTTP
    Servidor-->>Client: Envia resposta amb l'HTML + les capçaleres HTTP
{{< /mermaid >}}


- 1 connexió per al text + 1 per a cada recurs



### Codis d'estat

Codi  |  Decripció
--|--
  1XX | Missatges informatius  
2XX  |  OK
3XX  |  Redirects
4XX  |  Error en clients
5XX  |  Error de servidor



## Test Wizardzines


- Flashcards de la Julia Evans per comprovar els coneixements [https://flashcards.wizardzines.com/http/](https://flashcards.wizardzines.com/http/)
