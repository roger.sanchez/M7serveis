---
Title: Configuració Apache2
weight: 3
Description: "Servidors d'aplicacions web. Servei web. Apache"
---



## Apache2.conf

- Fitxer principal de configuració

### Directives principals:

|Directiva|Descripció|
|-----|------|
|ServerRoot "/etc/apache2"  | Directori arrel de la configuració del servidor Apache|
|Timeout 300    | Temps que triga Apache en donar un error si està esperant alguna cosa|
|KeepAlive On   | Connexions persistents, no tanca la connexió amb el client immediatament|
|StartServer   5    |Nombre de processos fill que es crearan al iniciar l'Apache|
|ErrorLog ${APACHE_LOG_DIR}/error.log      | On es troba el fitxer d'errors|
|LogLevel warn                             | Grau de detall dels errors|
|Include mods-enabled/*.load               | Incloure els mòduls activats|
|Include httpd.conf                        | Incloure configuració del fitxer httpd.conf|
|Include ports.conf                        | Incloure configuració de ports|
|Include conf.d/                           | Incloure configuracions addicionals|
|Include sites-enabled/   

### Seccions

- Les directives estan agrupades en tres seccions: :
	- Directives globals: controlen el funcionament del servidor Apache server, corresponen a configuració i optimització del servidor. No les tractarem (Section 1)
	- Directives que defineixen els parametres del servidor principal ('main' o 'default'), que respon a les peticions que no concorden amb cap virtual host. Aquestes directives donen els valors per defecte (default) per a tots els virtual host (si no es defineixen a dintre, agafen el valor que s'hagi posat a les directives de fora de qualsevol host virtual. No tocarem els valors per defecte.
	- Paràmetres dels virtual hTTPS
