---
Title: Serveis
weight: 1
Description: "SMX Sistemes Microinformàtics i xarxes. M7 Serveis"
---

## Estructura del mòdul SMX M7

{{< mermaid >}}
flowchart TB  
  subgraph START
  end
  subgraph UF1
  DHCP-->DNS
  end
  subgraph UF2
  correu-->FTP
  end
  subgraph UF3
  WEB-->PROXY
  WEB-->NAT
  end
  subgraph UF4
  SSH-->VPN
  end  
  subgraph END
  end
  START --> UF1
  START --> UF4
  UF1 --> UF2
  UF2 --> UF3  
  UF3 --> END
  UF4 --> END
{{< /mermaid >}}
