---
Title: Servei Web
weight: 1
Description: "Servidors d'aplicacions web. Servei web. Apache"
---


## Tipus de hosting

- Hosting intern
  - El servidor web està en la pròpia xarxa
- Hosting extern
  - El servidor està subcontractat fora de la xarxa



### Avanatages / inconvenients

#### Hosting intern

-  Costos: equip, manteniment / costos propis de la xarxa administrada
-  Avantatges: privacitat, confidencialitat, gestió (possibilitat d'afegir paquets,serveis, scripts, monitors, backups,gestors de logs,etc...)

---

#### Hosting extern
##### Avantatges

- Disponibilitat
- backups
- Redundancia
- Escalabilitat
- Software automàtic de gestió de paquets
- Cost del manteniment

##### Desavantatges

- El software automàtic sol ser una diana per els atacants
- No control del sistema



## Servidors web més utilitzats

- http://w3techs.com/technologies/overview/web_server/all


## Clients web disponibles per accedir al servei web:

- Client Web mode consola:  Podem fer servir el client "links" (sudo apt-get install links). Piqueu F9 per obtenir el menú. A partir d'aquí, configureu el que necessiteu. Per anar a un lloc web, piqueu 'g' ("go" en anglès). A l'opció "Setup" podeu canviar idioma i configurar proxy entre altres coses.
- Curl per a fer proves és molt potent
- Client Web gràfic: Firefox (win, linux), Internet Explorer (Win), Safari, etc.

- Si teniu problemes amb la pàgina web fent proves amb el client WEB, assegureu-vos que:
	- El client web no està configurat per passar pel proxy.
	- Que heu netejat la memòria cau (caché o historial recent) del navegador. Les pàgines estàtiques (index.html) i la validació d'usuari es recorda pel navegador.
